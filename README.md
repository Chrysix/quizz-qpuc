# Projet Quizz QPUC (Question pour un Champion)

Voici mon projet de Quizz que j'ai effectué pendant ma formation Developpeur Web/ Web Mobiles.

Celui-ci est inspiré du jeu Question pour un Champion, avec des modifications pour ce tenir au condition donné pour le Projet.

## ↓ Lien maquette ↓

https://www.figma.com/file/qSGYP75pMwOXsfWaYR5hy4/Quizz-QPUC-4-%C3%A0-la-suite


### ↓ Lien Site en ligne ↓


https://chrysix.gitlab.io/quizz-qpuc

let theme = document.querySelector(".theme");

let header = document.querySelector("header");

let page1 = document.querySelector(".page1");

let page2 = document.querySelector(".page2");

let page3 = document.querySelector(".page3");

//------------------↓-Page 1 caché-↓---------------------------------

theme.addEventListener("click", () => {

    header.style.display = "none"
    page1.style.display = "none"
    page2.style.display = "block"

})

//--------------------------↓-Question/Réponse-↓-------------------------------------

let theme1 = [{//--------------------------------------------------Theme 1------------------------------------------------------

    question: "Comment s’appellent les premières dents des enfants ?",
    reponse: {
        a: "Dents de sagesse",
        b: "Dents de lait",
        c: "Molaires",
        d: "Incisives",
    },

    correctAnswer: "b"

},

{
    question: "Quelle matière blanche recouvre la surface extérieure des dents ?",
    reponse: {
        a: "Dentine",
        b: "Collet",
        c: "Émail",
        d: "Kératine",
    },

    correctAnswer: "c"

},

{
    question: "Dans quelle muqueuse de la bouche sont implantées les dents ?",
    reponse: {
        a: "Gencive",
        b: "Gingivite",
        c: "Lèvres",
        d: "Palais",
    },

    correctAnswer: "a"

},
{
    question: "Quelle expression, dite loi du talion, incite à la vengeance ?",
    reponse: {
        a: "La vengence est un plat qui ce mange froid",
        b: "L'ambition et la vengeance ont toujours faim",
        c: "La vengeance est douce",
        d: "Œil pour œil, dent pour dent",
    },

    correctAnswer: "d"
},
{
    question: "Quel nom portent les 4 dents pointues qui servent à déchirer la viande ?",
    reponse: {
        a: "Molaires",
        b: "Canines",
        c: "Incisives",
        d: "Prémolaires",
    },

    correctAnswer: "b"

},
{
    question: "Quelles molaires apparaissent à l’âge adulte ?",
    reponse: {
        a: "Incisives",
        b: "Molaires",
        c: "Prémolaires",
        d: "Dents de sagesse",
    },

    correctAnswer: "d"

},
{
    question: "Quel film d’horreur américain montre la lutte entre un groupe de jeunes et un requin ?",
    reponse: {
        a: "Panique à bords",
        b: "La mer a des dents !",
        c: "Les dents de la mer",
        d: "Les dents des Océans",
    },

    correctAnswer: "c"

},
{
    question: "Avec les dents de sagesse, combien un adulte possède t-il de dents ?",
    reponse: {
        a: "28",
        b: "32",
        c: "43",
        d: "30",
    },

    correctAnswer: "b"

},
{
    question: "Quelle maladie infectieuse de la dent provoque un trou dans l’émail ?",
    reponse: {
        a: "Carrie",
        b: "Aphte",
        c: "Chancre",
        d: "Cavité",
    },

    correctAnswer: "a"

},
{
    question: "Par mâchoire combien a-t-on d’incisives ?",
    reponse: {
        a: "1",
        b: "2",
        c: "3",
        d: "4",
    },

    correctAnswer: "d"

},

];



// let theme2 = [{//--------------------------------------------------Theme 2------------------------------------------------------

//     question: "Comment te sens-tu ?",
//     reponse: "Super",
//     reponse: "Bien",
//     reponse: "pas bien",
//     reponse: "Bof",

// },

// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// },
// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// },
// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// }];

// let theme3 = [{//--------------------------------------------------Theme 3------------------------------------------------------

//     question: "Comment te sens-tu ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// },

// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// },
// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// },
// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// }];

// let themeMyst = [{//--------------------------------------------------Theme Mystère------------------------------------------------------

//     question: "Comment te sens-tu ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// },

// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// },
// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// },
// {
//     question: "Comment vous sentez-vous ?",
//     reponse1: "Super",
//     reponse2: "Bien",
//     reponse3: "pas bien",
//     reponse4: "Bof",

// }];


//----------------------------------------------------------------------------------------------------------------------


let enonceQ = document.querySelector(".bulleQ")


let enonceR = document.querySelector(".bulleR")

let skip = document.querySelector(".bulleSkip")

let enonceR1 = document.querySelector("#r1")

let enonceR2 = document.querySelector("#r2")

let enonceR3 = document.querySelector("#r3")

let enonceR4 = document.querySelector("#r4")

let point = document.querySelector(".point")

let pret = document.querySelector("#pret")


let homePage = document.querySelector(".homePage")
let paraScore = document.querySelector(".score")
let affichScore = document.querySelector(".affichScore")

function switchPageScore() {

    homePage.style.display = "none"
    page3.style.display = "block"
    
}


//--------------------------------------Test pour faire en sorte de gardé une valeur si on fait une réponse fausse--------------------------

// function scoreIncrement() {
    
//     if (score > scoreMemory) {
        
//         scoreMemory = score
        
//     }

//     score = 0;
// }

//------------------------------------------------------------------------------------------------------------------------------------------------


//----------------------------------↓-Question-↓---------------------------------------------------------

//----------------------------------------Switch Question----------------------------------
function quiz(index) {

    if (quizIndex <= theme1.length - 1) {
        enonceQ.textContent = theme1[index].question
        enonceR1.textContent = theme1[index].reponse.a
        enonceR2.textContent = theme1[index].reponse.b
        enonceR3.textContent = theme1[index].reponse.c
        enonceR4.textContent = theme1[index].reponse.d

        point.style.background = "#0734CF";
        point.textContent = "";


    } else {

        switchPageScore()
    }

}


let quizIndex = 0;
let score = 0;
// let scoreMemory = 0;



quiz(quizIndex);


enonceQ.style.display = "none"
enonceR1.style.display = "none"
enonceR2.style.display = "none"
enonceR3.style.display = "none"
enonceR4.style.display = "none"
skip.style.display = "none"

//----------------------------------------------verificateur-------------------------------------------

enonceR1.addEventListener("click", () => {
    isCorrect("a")
    
})


enonceR2.addEventListener("click", () => {
    isCorrect("b")
    
})


enonceR3.addEventListener("click", () => {
    isCorrect("c")
    
})


enonceR4.addEventListener("click", () => {
    isCorrect("d")
    
})

//---------------------------------Reponse false or true----------------------------------------

/**
 *
 * @param {boolean} answer
 */
function isCorrect(answer) {

    if (theme1[quizIndex].correctAnswer === answer) {

        console.log("True");
        // enonceR.classList.add(".correct") 
        point.style.background = "#1EE732";
        point.textContent = "Bien joué !";
        score = score
        score++
        
        affichScore.textContent = "Score : " + score;
        
        if (score === 4) {
            
            switchPageScore()
            paraScore.textContent = "Incroyable !! C'est un 4 à la suite ! Vous remportés " + score + " points !"

        } else {

            paraScore.textContent = "Bravo, vous remportés " + score + " points"
        }
        
    } else {

        console.log("false");
        point.style.background = "red";
        point.textContent = "Dommage";
        score = score
        score--
        if (score < 0) {
            
            score = 0
        }
        affichScore.textContent = "Score : " + score;
        // enonceR.classList.add(".false")

    }

    quizIndex++
    console.log(score);
    // console.log(scoreMemory);
    window.setTimeout(quiz, 1000, quizIndex);

};

//----------------------------------Button-Skip----------------------------------------------
skip.addEventListener("click", () => {

    score--
    affichScore.textContent = "Score : " + score;
    quizIndex++
    window.setTimeout(quiz, 500, quizIndex);
})

//----------------------------------Button-Start----------------------------------------------

let start = document.querySelector(".start")
let affichTime = document.querySelector(".affichTime p")



start.addEventListener("click", () => {

    enonceQ.style.display = "initial"
    enonceR1.style.display = "initial"
    enonceR2.style.display = "initial"
    enonceR3.style.display = "initial"
    enonceR4.style.display = "initial"
    skip.style.display = "initial"

    start.style.display = "none"
    pret.style.display = "none"



//------------------------------------------Timer--------------------------------------------

    let time = 31;
    let count;


    timedCount();

    function timedCount() {

        time = time - 1;
        count = setTimeout(timedCount, 1000);
        affichTime.textContent = time;

        if (time === 0) {

            switchPageScore()

        }
    }

})

//---------------------------------------------Bouton Réessayer-----------------------------------------

let retry = document.querySelector(".retry")

retry.addEventListener("click", () =>{

    document.location.reload();
})